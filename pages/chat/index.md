---
title: VeilidChat
description: VeilidChat is a proof of concept of the Veilid protocol and framework.
menu:
  main:
    weight: 5
weight: 1
layout: subpage
---

VeilidChat is a demo of the Veilid framework and protocol working.

We built it using the [Flutter](https://flutter.dev/) framework and the Veilid core code.

If you want to try out this proof of concept, please stay tuned for details on how to gain access.

### Source Code

The code for VeilidChat will be available at https://gitlab.com/veilid/veilidchat once we're recovered from DefCon.

### Support

<p>For app support, please email <a href="mailto:support@veilid.com">support@veilid.com</a></p>

